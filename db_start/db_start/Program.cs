﻿using System;
using System.Linq;

namespace db_start
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                //Plane plane1 = new Plane { Model = "Airbus" };
                //Plane plane2 = new Plane { Model = "Embrier" };
                //db.Add(plane1);
                //db.Add(plane2);

                //db.SaveChanges();
                Console.WriteLine("Changes saved");
                var planes = db.Planes.ToList();
                foreach (Plane p in planes)
                {
                    Console.WriteLine($"{p.Id} = {p.Model}");
                }
                Console.WriteLine("Finished");
                Console.Read();
            }
        }
    }
}
