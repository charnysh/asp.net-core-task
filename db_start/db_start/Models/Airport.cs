﻿using System;
using System.Collections.Generic;
using System.Text;

namespace db_start
{
    class Airport
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Code { get; set; }
    }
}
