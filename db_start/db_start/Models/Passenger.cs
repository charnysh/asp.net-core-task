﻿using System;
using System.Collections.Generic;
using System.Text;

namespace db_start
{
    class Passenger
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Flight { get; set; }
        public int SeatType { get; set; }
    }
}
