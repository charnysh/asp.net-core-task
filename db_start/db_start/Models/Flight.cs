﻿using System;
using System.Collections.Generic;
using System.Text;

namespace db_start
{
    class Flight
    {
        public int Id { get; set; }
        public int FromAirport { get; set; }
        public int ToAirport { get; set; }
        public int PlaneID { get; set; }
        public DateTime LeaveDate { get; set; }
        public DateTime ArriveDate { get; set; }
    }
}
