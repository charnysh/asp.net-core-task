﻿using System;
using System.Collections.Generic;
using System.Text;

namespace db_start
{
    class Seat
    {
        public int Id { get; set; }
        public string Class { get; set; }
        public bool isWindow { get; set; }
        public int PlaneID { get; set; }
    }
}
